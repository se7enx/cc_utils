<?php

/**
 * @package ccUtils
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 * @date    07 Feb 2018
 */

require 'autoload.php';

$cli = eZCLI::instance();

$scriptSettings = array(
    'description' => 'Move node',
    'use-session' => true,
    'use-modules' => true,
    'use-extensions' => true
);
$script = eZScript::instance($scriptSettings);
$script->startup();
$script->initialize();

// Get options
$options = $script->getOptions(
    '[node-id:][dst-node-id:]',
    '',
    array(
        'node-id'     => 'Node ID which need to be moved.',
        'dst-node-id' => 'New parent node node ID.'
    )
);

// Login as admin user
$ini           = eZINI::instance();
$userCreatorID = $ini->variable('UserSettings', 'UserCreatorID');
$user          = eZUser::fetch($userCreatorID);
if (( $user instanceof eZUser ) === false) {
    $cli->error('Cannot get user object by userID = "' . $userCreatorID . '". ( See site.ini [UserSettings].UserCreatorID )');
    $script->shutdown(1);
}
eZUser::setCurrentlyLoggedInUser($user, $userCreatorID);

$cli->output('Starting script...');
$startTime = microtime(true);

// Check arguments
$node = eZContentObjectTreeNode::fetch((int) $options['node-id']);
if ($node instanceof eZContentObjectTreeNode === false) {
    $cli->error( 'Can\'t fetch node to move (ID: ' . (int) $options['node-id'] . '). Please check node-id argument and try again.' );
    $script->shutdown(1);
}
$destNode = eZContentObjectTreeNode::fetch($options['dst-node-id']);
if ($destNode instanceof eZContentObjectTreeNode === false) {
    $cli->error( 'Can\'t fetch new parent node (ID: ' . (int) $options['dst-node-id'] . '). Please check dst-node-id argument and try again.' );
    $script->shutdown(1);
}

if ((int) $node->attribute('parent_node_id') === (int) $destNode->attribute('node_id')) {
    $cli->error( 'Can\'t move node to the same location where it is now.' );
    $script->shutdown(1);
}

$result = eZContentOperationCollection::moveNode(
    $node->attribute('node_id'),
    $node->attribute('contentobject_id'),
    $destNode->attribute('node_id')
);

$runtime = microtime(true) - $startTime;
$cli->output('Script run-time: ' . $runtime . ' sec.');
$script->shutdown(0);
