<?php

/**
 * @package ccUtils
 * @author  Chris Moutsos <chris@contextualcode.com>
 * @date    12 March 2019
 **/

require 'autoload.php';
$cli = eZCLI::instance();
$cli->setUseStyles(true);

$scriptSettings                   = array();
$scriptSettings['description']    = 'Imports alt tags from a csv file to Image attributes';
$scriptSettings['use-session']    = true;
$scriptSettings['use-modules']    = true;
$scriptSettings['use-extensions'] = true;

$script  = eZScript::instance($scriptSettings);
$script->startup();
$script->initialize();
$options = $script->getOptions('[filename:][nodeidcolindex:][alttextcolindex:][skipempty:][onlyempty:][usecolindexforempty:]',
    '',
    array(
        'filename'        => 'The file to read the csv from.',
        'nodeidcolindex'  => 'Zero-indexed number of which column contains the node ids.',
        'alttextcolindex' => 'Zero-indexed number of which column contains the alt text.',
        'onlyempty'       => 'Skip all non-empty new alt texts (to be used with usecolindexforempty).',
        'skipempty'       => 'If empty new alt texts should be skipped instead of set.',
        'usecolindexforempty' => 'If empty new alt text, use this column for the alt text.'
    )
);
foreach (array('filename', 'nodeidcolindex', 'alttextcolindex') as $k) {
    if (!isset( $options[$k])) {
        $cli->error('Please supply a `--' . $k. '` argument.');
        $script->shutdown(1);
    }
}

$ini           = eZINI::instance();
$userCreatorID = $ini->variable('UserSettings', 'UserCreatorID');
$user          = eZUser::fetch($userCreatorID);
if (($user instanceof eZUser) === false) {
    $cli->error('Cannot get user object by userID = "' . $userCreatorID . '". ( See site.ini [UserSettings].UserCreatorID )');
    $script->shutdown(1);
}
eZUser::setCurrentlyLoggedInUser($user, $userCreatorID);

$filename         = $options['filename'];
$nodeIdColIndex   = $options['nodeidcolindex'];
$altTextColIndex  = $options['alttextcolindex'];
$skipEmpty        = $options['skipempty'];
$onlyEmpty        = $options['onlyempty'];
$emptyColIndex    = $options['usecolindexforempty'];

$csvData = array_map('str_getcsv', file($filename));

$count = count($csvData);
foreach ($csvData as $key => $d) {
    $nodeId = $d[$nodeIdColIndex];
    $newAltText = $d[$altTextColIndex];

    if ($onlyEmpty && !empty($newAltText)) {
        continue;
    }

    if ($skipEmpty && empty($newAltText)) {
        $cli->output("Skipping empty alt text for node id $nodeId");
        continue;
    }

    if ($emptyColIndex && empty($newAltText)) {
        $newAltText = $d[$emptyColIndex];
    }

    $node = eZContentObjectTreeNode::fetch($nodeId);
    if ($node instanceof eZContentObjectTreeNode === false) {
        continue;
    }

    $object = $node->object();
    if ($object instanceof eZContentObject === false) {
        continue;
    }
    $objectId = $object->attribute('id');

    if( $key % 100 === 0 ) {
        $memoryUsage = number_format( memory_get_usage( true ) / ( 1024 * 1024 ), 2 );
        $output = number_format( $key / $count * 100, 2 ) . '% (' . ( $key + 1 ) . '/' . $count . ')';
        $output .= ', Memory usage: ' . $memoryUsage . ' Mb';
        $cli->output( $output );
    }

    $dataMap = $object->attribute('data_map');
    foreach($dataMap as $attr) {
        if($attr->attribute('data_type_string') != eZImageType::DATA_TYPE_STRING) {
            continue;
        }

        $image = $attr->attribute('content');
        $oldAltText = $image->attribute('alternative_text');
        if ($oldAltText === $newAltText) {
            continue;
        }

        $cli->output("Setting alt text of node id $nodeId object id $objectId to $newAltText");

        $image->setAttribute('alternative_text', $newAltText);
        $image->store($attr);
    }

    eZContentObject::clearCache($objectId);
    $object->resetDataMap();
    unset($object);
}

$cli->output('Done!');
$script->shutdown(0);

