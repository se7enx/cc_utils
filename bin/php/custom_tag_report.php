<?php

/**
 * @package ccUtils
 * @author  Chris Moutsos <chris@contextualcode.com>
 * @date    15 Apr 2019
 **/

require 'autoload.php';
$cli = eZCLI::instance();
$cli->setUseStyles(true);

$scriptSettings                   = array();
$scriptSettings['description']    = 'Reports usages of custom tags in content';
$scriptSettings['use-session']    = true;
$scriptSettings['use-modules']    = true;
$scriptSettings['use-extensions'] = true;

$script  = eZScript::instance($scriptSettings);
$script->startup();
$script->initialize();
$options = $script->getOptions('[customtagname:][show_nodes][show_number:]',
    '',
    array(
        'customtagname' => 'The custom tag name.',
        'show_nodes' => 'Output nodes.',
        'show_number' => 'If verbose, number of items to show',
    )
);
if (!isset($options['customtagname'])) {
    $cli->error('Please supply a `--customtagname` argument.');
    $script->shutdown(1);
}
$verbose = (isset($options['show_nodes']) && $options['show_nodes']);

$ini           = eZINI::instance();
$userCreatorID = $ini->variable('UserSettings', 'UserCreatorID');
$user          = eZUser::fetch($userCreatorID);
if (($user instanceof eZUser) === false) {
    $cli->error('Cannot get user object by userID = "' . $userCreatorID . '". ( See site.ini [UserSettings].UserCreatorID )');
    $script->shutdown(1);
}
eZUser::setCurrentlyLoggedInUser($user, $userCreatorID);

$db = eZDB::instance();
$customTagName = $options['customtagname'];
$query = 'SELECT DISTINCT ezcontentobject_tree.main_node_id, ezcontentobject.name FROM ezcontentobject_attribute, ezcontentobject, ezcontentobject_tree WHERE data_type_string="ezxmltext" AND data_text LIKE "%custom name=\"' . $customTagName . '\"%" AND ezcontentobject_attribute.version=ezcontentobject.current_version AND ezcontentobject.id=ezcontentobject_tree.contentobject_id AND ezcontentobject_attribute.contentobject_id=ezcontentobject.id';
$rows = $db->arrayQuery($query);
if (!is_array($rows)) {
    $rows = array();
}
$count = count($rows);
$data = array();
foreach ($rows as $row) {
    $data[] = array(
        'main_node_id' => $row['main_node_id'],
        'name'         => $row['name']
    );
}
$cli->output($customTagName);
$cli->output($count);
if ($verbose) {
    if (!isset($options['show_number']) || empty($options['show_number'])) {
        $dataToShow = $data;
    }
    else {
        $dataToShow = array_slice($data, 0, $options['show_number']);
    }
    $nodeStr = '';
    $nameStr = '';
    foreach ($dataToShow as $d) {
        $nodeStr .= $d['main_node_id'] . ';';
        $nameStr .= $d['name'] . ';';
    }
    $cli->output($nodeStr);
    $cli->output($nameStr);
}

$script->shutdown(0);
